from rest_framework import serializers
from .models import *


class habitacion_serializer(serializers.ModelSerializer):
    class Meta:
        model = habitacion
        # fields = "__all__"
        exclude = ["created_date", "updated_date"]

class cliente_serializer(serializers.ModelSerializer):
    class Meta:
        model = cliente
        # fields = "__all__"
        exclude = ["created_date", "updated_date"]

class reserva_maestro_serializer(serializers.ModelSerializer):
    class Meta:
        model = reserva_maestro
        # fields = "__all__"
        exclude = ["created_date", "updated_date"]


class reserva_detalle_serializer(serializers.ModelSerializer):
    class Meta:
        model = reserva_detalle
        # fields = "__all__"
        exclude = ["created_date", "updated_date"]

class factura_serializer(serializers.ModelSerializer):
    class Meta:
        model = factura
        # fields = "__all__"
        exclude = ["created_date", "updated_date"]

#* SERIALIZER PARA DETALLAR INFORMACION
class reserva_detalle_mostrar(serializers.ModelSerializer):
    habitacion_id = habitacion_serializer()
    class Meta:
        model = reserva_detalle
        # fields = "__all__"
        exclude = ["created_date", "updated_date"]

class reserva_maestro_mostrar_serializer(serializers.ModelSerializer):
    cliente_id = cliente_serializer()

    class Meta:
        model = reserva_maestro
        # fields = "__all__"
        exclude = ["created_date", "updated_date"]

    