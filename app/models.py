from django.db import models

# Create your models here.

# -----------------------------------------------------------------------------------------
class Base(models.Model):
    """Modelo abstracto base del que heredan todos los modelos para manejar la auditoria"""
    created_date = models.DateTimeField('Fecha de registro', auto_now_add=True)
    updated_date = models.DateTimeField('Fecha de modificación', auto_now=True)
    #estado = models.BooleanField('Estado', default=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        super(Base, self).save(*args, **kwargs)
# -----------------------------------------------------------------------------------------

class habitacion(Base):
    nro_piso = models.PositiveIntegerField("Nro de piso")
    nro_habitacion = models.PositiveIntegerField("Nro de habitacion")
    precio = models.DecimalField(max_digits=5, decimal_places=2)
    nro_camas = models.PositiveIntegerField("Nro de camas")
    con_banio_priv = models.BooleanField("Con baño privado", default=True)

    def __str__(self):
        return str(self.nro_habitacion) + " Piso " + str(self.nro_piso)
    
    class Meta:
        ordering = ('nro_piso',)

class cliente(Base):
    nombres = models.CharField("Nombres", max_length=200)
    apellidos = models.CharField("Apellidos", max_length=200)
    doc_id = models.CharField("Documento de Identidad", max_length=20, unique=True)
    email = models.EmailField("Email", max_length=100)
    nacionalidad = models.CharField("Nacionalidad", max_length=50, null=True)

    def __str__(self):
        return self.nombres +" "+ self.apellidos

    class Meta:
        ordering = ('apellidos',)

class reserva_maestro(Base):
    ESTADO_RESERVA = (
        ("PENDIENTE","PENDIENTE"),
        ("PAGADO","PAGADO"),
        ("ELIMINADO","ELIMINADO"),
    )
    cliente_id = models.ForeignKey(cliente, on_delete=models.CASCADE)
    estado = models.CharField("Estado de la reserva", max_length=10, choices=ESTADO_RESERVA)
    monto_total = models.DecimalField(max_digits=10, decimal_places=2, null=True)

    def __str__(self):
        return str(self.cliente_id) + " - " + self.estado + " --> " + str(self.created_date).split(" ")[0]

    class Meta:
        ordering = ('updated_date',)

class reserva_detalle(Base):
    reserva_maestro_id = models.ForeignKey(reserva_maestro, on_delete=models.CASCADE)
    habitacion_id = models.ForeignKey(habitacion, on_delete=models.CASCADE)
    check_in_date = models.DateField("Check in")
    check_out_date = models.DateField("Check out")

    def __str__(self):
        return str(self.reserva_maestro_id) + " - " + str(self.habitacion_id) + "-->" + str(self.check_in_date)

    class Meta:
        ordering = ('updated_date',)

class factura(Base):
    reserva_maestro_id = models.OneToOneField(reserva_maestro, on_delete=models.PROTECT)
    metodo_pago = models.CharField("Metodo de pago", max_length=20, null=True)
    fecha_pago = models.DateTimeField('Fecha de Pago', auto_now_add=True)
    monto_total = models.DecimalField(max_digits=10, decimal_places=2)
    def __str__(self):
        return str(self.reserva_maestro_id)

    class Meta:
        ordering = ('created_date',)

