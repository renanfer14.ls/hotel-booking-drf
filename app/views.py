from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveUpdateAPIView
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from django.db.models import Sum
from django.db import transaction
from .models import *
from .serializers import *
# Create your views here.

#* --------------------------------------------------------------------
"""Clases para realizar un CRUD con el model Cliente
    
"""
class cliente_list(ListCreateAPIView):
    queryset = cliente.objects.all()
    serializer_class = cliente_serializer

class cliente_rud(RetrieveUpdateDestroyAPIView):
    queryset = cliente.objects.all()
    serializer_class = cliente_serializer

#* --------------------------------------------------------------------
"""Clases para realizar un CRUD con el model Habitacion
    
"""
class habitacion_list(ListCreateAPIView):
    queryset = habitacion.objects.all()
    serializer_class = habitacion_serializer

class habitacion_rud(RetrieveUpdateDestroyAPIView):
    queryset = habitacion.objects.all()
    serializer_class = habitacion_serializer

#* --------------------------------------------------------------------
"""Clases para realizar un CRUD con el model Reserva Maestro
    
"""
class reserva_maestro_list(ListCreateAPIView):
    queryset = reserva_maestro.objects.all()
    serializer_class = reserva_maestro_serializer

class reserva_maestro_rud(RetrieveUpdateDestroyAPIView):
    queryset = reserva_maestro.objects.all()
    serializer_class = reserva_maestro_serializer

#* --------------------------------------------------------------------
"""Clases para realizar un CRUD con el model Reserva Detalle
    
"""
class reserva_detalle_list(ListCreateAPIView):
    queryset = reserva_detalle.objects.all()
    serializer_class = reserva_detalle_serializer

class reserva_detalle_rud(RetrieveUpdateDestroyAPIView):
    queryset = reserva_detalle.objects.all()
    serializer_class = reserva_detalle_serializer


#* --------------------------------------------------------------------
"""Clases para realizar un CRUD con el model Factura
    
"""
class factura_list(ListCreateAPIView):
    queryset = factura.objects.all()
    serializer_class = factura_serializer

class factura_rud(RetrieveUpdateDestroyAPIView):
    queryset = factura.objects.all()
    serializer_class = factura_serializer

#* ----------------- Procesos -------------------------- 
class reserva_habitacion(APIView):
    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'nombres': openapi.Schema(type=openapi.TYPE_STRING, description='Nombres', example="Juan"),
            'apellidos': openapi.Schema(type=openapi.TYPE_STRING, description='Apellidos', example="Perez"),
            'doc_id': openapi.Schema(type=openapi.TYPE_STRING, description='Doc. identidad', example="66351273"),
            'email': openapi.Schema(type=openapi.FORMAT_EMAIL, description='email', example="juan@perez.com"),
            'nacionalidad': openapi.Schema(type=openapi.TYPE_STRING, description='nacionalidad', example="Colombiana"),
            'estado': openapi.Schema(type=openapi.TYPE_STRING, description='Estado de la reserva', example="PAGADO"),
            'metodo_pago': openapi.Schema(type=openapi.TYPE_STRING, description='Metodo de pago', example="TARJETA MASTERCARD"),

            'reservas': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                                                            type=openapi.TYPE_OBJECT,
                                                                            properties={
                    "habitacion_id": openapi.Schema(type=openapi.TYPE_INTEGER, description='PK de la habitacion', example=1),
                    "check_in_date": openapi.Schema(type=openapi.TYPE_STRING, description='Fecha entrada', example="2022-01-01"),
                    "check_out_date": openapi.Schema(type=openapi.TYPE_STRING, description='Fecha Salida', example="2022-01-10")
                }))
        }))
    def post(self, request):
        #? Revisar la informacion del cliente, nuevo? registrar: recuperar
        with transaction.atomic():
            # Recuperar o registrar al cliente
            try:
                cliente_rec = cliente.objects.get(doc_id=request.data['doc_id'])
            except cliente.DoesNotExist:            
                serializer_cliente = cliente_serializer(data=request.data)
                if serializer_cliente.is_valid(raise_exception=True):
                    serializer_cliente.save()
                cliente_rec = cliente.objects.get(doc_id=request.data['doc_id'])           

            # generar reserva maestro
            data = {
                "cliente_id": cliente_rec.id,
                "estado": request.data['estado'],
            }
            serializer = reserva_maestro_serializer(data=data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                # generar reserva detalles
                for i in request.data['reservas']:
                    det_data = {
                        "reserva_maestro_id": serializer.data['id'],
                        "habitacion_id": i['habitacion_id'],
                        "check_in_date": i['check_in_date'],
                        "check_out_date": i['check_out_date'],
                    }
                    serializer_det = reserva_detalle_serializer(data=det_data)
                    if serializer_det.is_valid(raise_exception=True):
                        serializer_det.save()

            # recuperamos el maestro
            detalle_maestro = reserva_maestro.objects.get(id=serializer.data['id'])
            # recuperamos los detalles de la reserva
            detalles_reserva = reserva_detalle.objects.filter(reserva_maestro_id=serializer.data['id'])
            # calculamos el monto total
            total_costo = 0
            for i in detalles_reserva:
                precio_habitacion = i.habitacion_id.precio
                total_dias = i.check_out_date - i.check_in_date
                total_costo+=total_dias.days * precio_habitacion
            
            # Actualizar el campo de monto total
            detalle_maestro.monto_total = total_costo
            detalle_maestro.save()
            #? Gererar la factura si el flag 'estado' viene con el valor 'PAGADO' 
            if request.data['estado'] == 'PAGADO':
                billing_data = {
                    "reserva_maestro_id": serializer.data['id'],
                    "metodo_pago": request.data['metodo_pago'],
                    "fecha_pago": datetime.datetime.now(),
                    "monto_total": total_costo
                }
                paym_serializer = factura_serializer(data=billing_data)
                if paym_serializer.is_valid(raise_exception=True):
                    paym_serializer.save()

            # Procesamiento de respuesta
            data_return = {
                "reserva": reserva_maestro_mostrar_serializer(detalle_maestro).data,
                "detalles_reserva": reserva_detalle_mostrar(detalles_reserva, many=True).data,
                "with_payment": True if request.data['estado']=='PAGADO' else False,
                "payment_data": paym_serializer.data if request.data['estado']=='PAGADO' else None
            }
            return Response(data_return, status=status.HTTP_201_CREATED)
        
class pago_reserva(APIView):
    @swagger_auto_schema(request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'metodo_pago': openapi.Schema(type=openapi.TYPE_STRING, description='Metodo de pago', example="Efectivo")
        }))
    def put(self, request, id_reserva_maestro):
        with transaction.atomic():
            # Actualizar el estado de la reserva
            recuperar_reserva = reserva_maestro.objects.get(id=id_reserva_maestro)
            recuperar_reserva.estado = 'PAGADO'
            recuperar_reserva.save()
            # Recuperar detalles de la reserva
            detalles_reserva = reserva_detalle.objects.filter(reserva_maestro_id=recuperar_reserva.id)
            # calcular el costo total de la reserva
            total_costo = 0
            for i in detalles_reserva:
                precio_habitacion = i.habitacion_id.precio
                total_dias = i.check_out_date - i.check_in_date
                total_costo+=total_dias.days * precio_habitacion
            # generar factura
            billing_data = {
                "reserva_maestro_id": recuperar_reserva.id,
                "metodo_pago": request.data['metodo_pago'],
                "fecha_pago": datetime.datetime.now(),
                "monto_total": total_costo
            }
            paym_serializer = factura_serializer(data=billing_data)
            if paym_serializer.is_valid(raise_exception=True):
                paym_serializer.save()
            # Procesamiento de respuesta
            data_return = {
                "reserva": reserva_maestro_mostrar_serializer(recuperar_reserva).data,
                "detalles_reserva": reserva_detalle_mostrar(detalles_reserva, many=True).data,
                "with_payment": True,
                "payment_data": paym_serializer.data
            }
            return Response(data_return, status=status.HTTP_200_OK)

class eliminar_reserva(APIView):
    def delete(self, request, id_reserva_maestro):
        with transaction.atomic():
            try:
                recuperar_reserva = reserva_maestro.objects.get(id=id_reserva_maestro)
                recuperar_reserva.estado = 'ELIMINADO'
                recuperar_reserva.save()
                return Response({
                    "message": "deleted successfully",
                }, status=status.HTTP_200_OK)
            except reserva_maestro.DoesNotExist:
                return Response({
                    "message": "Reserva maestro not found",
                }, status=status.HTTP_404_NOT_FOUND)

class listar_reserva_maestro_cliente(APIView):
    def get(self, request, id_cliente):
        try:
            cliente_rec = cliente.objects.get(id=id_cliente)
            reservas = reserva_maestro.objects.filter(cliente_id=id_cliente)    
            lista = []
            for i in reservas:
                data_st = {}
                serializer = reserva_maestro_mostrar_serializer(i)
                detalle_reservas = reserva_detalle.objects.filter(reserva_maestro_id=i.id)
                serializer_detalle = reserva_detalle_mostrar(detalle_reservas, many=True)
                data_st['reserva_maestro'] = serializer.data
                data_st['reserva_detalle'] = serializer_detalle.data
                lista.append(data_st)
            return Response(lista, status=status.HTTP_200_OK)
        except cliente.DoesNotExist:
            return Response({
                    "message": "Cliente not found",
                }, status=status.HTTP_404_NOT_FOUND)

class listar_reserva_detalle_cliente(APIView):
    def get(self, request, id_reserva_maestro):
        try:
            reserva_maes = reserva_maestro.objects.get(id=id_reserva_maestro)
            reservas = reserva_detalle.objects.filter(reserva_maestro_id=id_reserva_maestro)
            serializer = reserva_detalle_mostrar(reservas, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except reserva_maestro.DoesNotExist:
            return Response({
                    "message": "Reserva maestro not found",
                }, status=status.HTTP_404_NOT_FOUND)
