from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(cliente)
admin.site.register(habitacion)
admin.site.register(factura)
admin.site.register(reserva_maestro)
admin.site.register(reserva_detalle)

