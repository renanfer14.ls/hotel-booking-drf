
from django.urls import path
from .views import *
urlpatterns = [
    path('clientes', cliente_list.as_view(), name= 'cliente_list'),
    path('clientes/<int:pk>', cliente_rud.as_view(), name= 'colegio_rud'),

    path('habitaciones', habitacion_list.as_view(), name= 'habitacione_list'),
    path('habitaciones/<int:pk>', habitacion_rud.as_view(), name= 'habitacione_rud'),

    path('reserva-maestro', reserva_maestro_list.as_view(), name= 'reserva_maestro_list'),
    path('reserva-maestro/<int:pk>', reserva_maestro_rud.as_view(), name= 'reserva_maestro_rud'),

    path('reserva-detalle', reserva_detalle_list.as_view(), name= 'reserva_detalle_list'),
    path('reserva-detalle/<int:pk>', reserva_detalle_rud.as_view(), name= 'reserva_detalle_rud'),

    path('factura', factura_list.as_view(), name= 'factura_list'),
    path('factura/<int:pk>', factura_rud.as_view(), name= 'factura_rud'),

    #? Procesos
    path("proceso/reserva", reserva_habitacion.as_view()),
    path("proceso/pagar-reserva/<int:id_reserva_maestro>", pago_reserva.as_view()),
    path("proceso/eliminar-reserva/<int:id_reserva_maestro>", eliminar_reserva.as_view()),
    path("proceso/listar/reserva-cliente/<int:id_cliente>", listar_reserva_maestro_cliente.as_view()),
    path("proceso/listar/reserva-detalle-cliente/<int:id_reserva_maestro>", listar_reserva_detalle_cliente.as_view()),   
]