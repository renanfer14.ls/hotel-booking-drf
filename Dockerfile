FROM python:3.8
ENV PYTHONBUFERRED 1

RUN mkdir /code
WORKDIR /code

COPY . /code


RUN python3 -m pip install -r requirements.txt