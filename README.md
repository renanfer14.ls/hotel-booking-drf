  # Sistema de reserva de un hotel

<hr>


[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

<hr>

<p align="center"> Sistema desarrollado para el manejo, gestion y mantenimiento de reservas de un hotel
    <br> 
</p>

# Contenido

- [Acerca del proyecto](#acerca)
- [Autor](#autor)
- [Deployment](#deployment)
- [Modo de uso](#uso)
- [Justificacion de endpoint propuestos](#justificacion)

## Acerca del proyecto <a name = "acerca"></a>

### **Herramientas utilizadas para el desarrollo del sistema**
* _[Python v3.8](https://www.python.org/)_
* _[Django v3.2](https://www.djangoproject.com)_
* _[Django Rest Framework v3.12](https://www.django-rest-framework.org/)_
* _[PostrgreSQL v12](hhttps://www.postgresql.org//)_
* _[Docker](https://www.docker.com)_


## Autor <a name = "autor"></a>

- [@renanfer14.ls](https://gitlab.com/renanfer14.ls)

## Prerequisitos <a name = "prerequisitos"></a>

Para la ejecucion del proyecto se necesitan las herramientas descritas anteriormente

## Despliegue
A continuacion se explica el proceso para realizar el despliegue del proyecto

Como primer paso crear un archivo .env, pegar el contenido del archivo .env.example y editar los valores por defecto
```
SECRET_KEY=django-insecure-d3l#h_a4&-k^d=x8tp63uq%rb0-@q4i3w5tseu)&9$ax6%z)0*
DJANGO_DEBUG=True

POSTGRES_DB=dbname
POSTGRES_USER=user
POSTGRES_PASSWORD=password
DB_HOST=db
DB_PORT=5432
```

Una vez listo, ejecutamos:

```
docker-compose build
```

Seguidamente de:
```
docker-compose run web migrate
```
Si se desea agregar un usuario para interactuar con el panel administrador, ejecutar el siguiente comando y completar los solicitado
```
docker-compose run web createsuperuser
```

Finalmente, para hacer un deploy al proyecto, ejecutar:
```
docker-compose up
```

## Modo de uso <a name="uso"></a>
Una vez que este en ejecucion, abrir el navegador con la direccion [127.0.0.1:8000](127.0.0.1:8000) el cual mostrara los endpoints creados para el proposito del proyecto e interactuar con ellos.

<hr>

## Justificacion de endpoints propuestos <a name="justificacion"></a>

- Los siguientes edpoints se generaron de manera que su uso esta orientado basicamente a un CRUD para cada modelo/tabla de la base de datos.
```
- /api/v1.0/clientes/
- /api/v1.0/clientes/<id>
- /api/v1.0/habitaciones/
- /api/v1.0/habitaciones/<id>
- /api/v1.0/reserva-maestro/
- /api/v1.0/reserva-maestro/<id>
- /api/v1.0/reserva-detalle/
- /api/v1.0/reserva-detalle/<id>
- /api/v1.0/factura/
- /api/v1.0/factura/<id>
```
<hr>

## Los siguientes edpoints corresponden a procesos como tal, los cuales son:

<hr>

```
proceso/reserva
```
Este endpoint sirve para realizar el registro de una reserva, comenzando desde el punto de registrar a un nuevo cliente o recuperarlo en caso que ya exista en nuestra base de datos, seguidamente la reserva se efectua con los datos del cliente y las habitaciones que demande dicho cliente, si este desea cancelar el monto total de la reserva entonces se genera una factura con todos los detalles de la operacion, en caso de no realizar el pago, la reserva se mantiene con un estado de PENDIENTE informando el monto total que se debe pagar.

<hr>

```
proceso/pagar-reserva/<id_reserva_maestro>
```
Este endpoint se encarga de realizar el pago de una reserva PENDIENTE, generando la factura correspondiente con los datos del cliente.

<hr>

```
proceso/eliminar-reserva/<id_reserva_maestro>
```
Este endpoint se encarga de realizar el cambio del estado de una reserva a ELIMINADO.

<hr>

```
proceso/listar/reserva-cliente/<id_cliente>
```
Endpoint dedicado a la consulta de todas las reservas de un cliente con los detalles correspondientes.


```
proceso/listar/reserva-detalle-cliente/<id_reserva_maestro>
```
Endpoint dedicado mostrar los detalles de una determinada reserva.

